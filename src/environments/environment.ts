// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCMQw-6o3ETaWTU8PTcmMoPybcJzHRPeoA',
    authDomain: 'contacts-cd0c4.firebaseapp.com',
    databaseURL: 'https://contacts-cd0c4.firebaseio.com',
    projectId: 'contacts-cd0c4',
    storageBucket: 'contacts-cd0c4.appspot.com',
    messagingSenderId: '264060420777'
  }
};
