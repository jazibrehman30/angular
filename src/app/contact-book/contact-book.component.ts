import { Component, OnInit } from '@angular/core';
import { FilterPipe } from '../filter.pipe';
import { AngularFireDatabase, FirebaseListObservable,FirebaseObjectObservable } from 'angularfire2/database';
import { AngularFireAuth } from "angularfire2/auth";
import { AuthService } from '../shared/auth/auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-contact-book',
  templateUrl: './contact-book.component.html',
  styleUrls: ['./contact-book.component.css']
})
export class ContactBookComponent implements OnInit {
  name: string;
  email: string;
  phone: string;
  contactKey: string;
  editIndex: number;

  public isLoggedIn;
  


  //this funtcion clears all data in input fields
  clearForm () {
    this.name = "";
    this.email = "";
    this.phone = "";
    this.contactKey = "";
  }
  userName:string;
  items: FirebaseListObservable<any>;

  //constructor
  constructor(db: AngularFireDatabase, private afAuth: AngularFireAuth, private authService: AuthService, private router: Router) {
    
    authService.isAuthenticated()
    .subscribe(
      success => this.isLoggedIn = success
    );

    //this creats the list in database where we will store our data,
    //and the parameter is the user ID of current user
    this.items = db.list(this.afAuth.auth.currentUser.uid);
    
    this.name = "";
    this.email = "";
    this.phone = "";
    this.contactKey = "";
  }


  logout() {
    this.authService.logout();
    this.router.navigate(['/signin']);
  }

  contacts() {
    this.router.navigate(['/contacts']);
  }

  //this function adds data to firebase, it takes data from fields as parameters
  addItem(newName: string, newEmail: string, newPhone) {
    this.items.push({ text: newName, newEmail, newPhone });
  }

  //this function is for updating the selected data, it also takes data from fields as parameters
  updateItem(newName: string, newEmail: string, newPhone ) {
    this.items.update(this.contactKey, { text: newName, newEmail, newPhone });
  }

  //this function deletes the selected card 
  deleteItem() {    
    this.items.remove(this.contactKey); 
  }
  
  //this function loads the selected data to input fields for updating
  cardData(item, i, key: string ) {
    this.name = item.text;
    this.email = item.newEmail;
    this.phone = item.newPhone;
    this.contactKey = key;
    this.editIndex = i;
  }
  ngOnInit() {
  }

}
