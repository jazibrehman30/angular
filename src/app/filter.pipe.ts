import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(names: any, term: any): any {
  //if search box is empty
      if(term == undefined) return names;
  //if search box is not empty
      return names.filter(function(contact){
      return contact.name.toLowerCase().includes(term.toLowerCase())
       ||
       contact.email.toLowerCase().includes(term.toLowerCase())
       ||
       contact.phone.toLowerCase().includes(term.toLowerCase());
       
        
        
      })
  }

}
