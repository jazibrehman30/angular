import { AppComponent } from './app.component';
import { ContactBookComponent } from './contact-book/contact-book.component';
import { SigninComponent } from './signin/signin.component';
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './shared/auth/auth.guard';
import { AuthService } from './shared/auth/auth.service';

export const router: Routes = [
    { path: '', redirectTo:'signin', pathMatch: 'full' },
    { path: 'signin', component: SigninComponent },
   { path: 'contacts', component: ContactBookComponent, canActivate: [AuthGuard] }
];

export const routes: ModuleWithProviders = RouterModule.forRoot(router);