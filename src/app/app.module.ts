import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from '../environments/environment';
import { FilterPipe } from './filter.pipe';
import { ContactBookComponent } from './contact-book/contact-book.component';
import { SigninComponent } from './signin/signin.component';
import { AuthService } from './shared/auth/auth.service';
import { routes } from './app.routes';
import { AuthGuard } from './shared/auth/auth.guard';

@NgModule({
  declarations: [
    AppComponent,
    FilterPipe,
    ContactBookComponent,
    SigninComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase, 'contact_list'), // imports firebase/app needed for everything
    AngularFireDatabaseModule, // imports firebase/database, only needed for database features
    AngularFireAuthModule,
    routes
    ],
  providers: [AuthService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
