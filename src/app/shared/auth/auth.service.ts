import { Injectable } from '@angular/core';
import { AngularFireAuth } from "angularfire2/auth";
import * as firebase from 'firebase/app';
import { Observable } from "rxjs/Observable";
import { CanActivate } from '@angular/router';

@Injectable()
export class AuthService {

  public user: Observable<firebase.User>;
  constructor(private afAuth: AngularFireAuth) {
    this.user = afAuth.authState;
   }

  login(email, password): Observable<any> {
    return Observable.fromPromise(
      this.afAuth.auth.signInWithEmailAndPassword(email, password)
    );
  }
  signupUser(email: string, password: string){
    console.log('service triggered');
    var subscription = Observable.fromPromise(firebase.auth().createUserWithEmailAndPassword(email, password));
        subscription.subscribe(firebaseUser => console.log(':)'),
                           error => console.log(error));
                           return false;
}

  isAuthenticated(): Observable<boolean> {
    return this.user.map(user => user && user.uid !== undefined);
  }

  logout() {
    this.afAuth.auth.signOut();
  }

}
