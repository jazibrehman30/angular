import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { AngularFireAuth } from 'angularfire2/auth';
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/do';
import { AuthService } from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(
        private af: AuthService,
        private router: Router,
        private auth: AngularFireAuth
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.af.user
                      .map(auth => auth != null)      // null means not authenticated
                      .do(isAuthenticated => {   // change routes if not authenticated
                         if(!isAuthenticated) {
                           this.router.navigate(['/signin']);
                         }
                      });
    
    }
}