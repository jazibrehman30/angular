import { Component, OnInit } from '@angular/core';
import { AuthService } from '../shared/auth/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css', 
  'assets/bootstrap/css/bootstrap.min.css', 
  'assets/font-awesome/css/font-awesome.min.css',
  'assets/css/form-elements.css',
  'assets/css/style.css'
]
})
export class SigninComponent implements OnInit {

  public user$ = this.authService.user;

  myName: string;  
  constructor(
    private af: AngularFireAuth, 
    private route: ActivatedRoute, 
    private authService: AuthService, 
    private router: Router) { }

  email: string;
  password: string;
  msg: string;
  user = this.af.authState;


  signup(email, password) {
    this.af.auth.createUserWithEmailAndPassword(email, password).then(
      res => {
        this.user.subscribe(user => {
          if (user != undefined && user != null) {
            user.sendEmailVerification().then(
              resolve => {
                this.af.auth.signOut();
                location.reload();
                this.router.navigate(['/signin'] , {relativeTo: this.route});
              }
            );
          }
        });
      }
    )
      .catch(error => this.msg = error.message);
  }


  login(email, password){
    this.authService.login(email, password)
    .subscribe(
      success => this.router.navigate(['/contacts']),
      error => alert(error)
    );
  }

  ngOnInit() {
  }

}
